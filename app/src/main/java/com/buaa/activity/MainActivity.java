package com.buaa.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.buaa.adapter.MyAdapter;
import com.buaa.util.Globals;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private TextView topview;
    private ListView mylist;
    private List<Map<String,Object>> values;
    private MyAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Globals.init(this);
        setContentView(R.layout.activity_main);
        topview= (TextView) findViewById(R.id.top_view);
        mylist= (ListView) findViewById(R.id.mylist);
        values=new ArrayList<>();
        //String path= Environment.getExternalStorageDirectory().getAbsolutePath();
        String path="/";
        loadFiles(path);
        adapter=new MyAdapter(this,values);
        mylist.setAdapter(adapter);
        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> map = values.get(position);
                boolean isfile = (boolean) map.get("isfile");
                File file = new File(map.get("fullpath").toString());
                if (!isfile) {
                    String path = map.get("fullpath").toString();
                    values.clear();
                    //Log.i("xxx",file+"?????????????????????????");
                    Map<String, Object> parentmap=null;
                    if (!file.getAbsolutePath().equals("/")) {
                        parentmap= new HashMap<String, Object>();
                        parentmap.put("fullpath", file.getParent());
                        parentmap.put("hasparent", "true");
                        parentmap.put("extname", "open_dir");
                        parentmap.put("filename", "返回上一级");
                        parentmap.put("isfile", false);
                        values.add(parentmap);
                    }
                    loadFiles(path);
                    adapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("文件信息");
                    builder.setMessage("文件大小：" + file.length() + "\r\n" + "文件名：" + file.getName());
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                }

            }
        });
        mylist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                Map<String, Object> map = values.get(position);
                boolean isfile = (boolean) map.get("isfile");
                final String filename=map.get("fullpath").toString();
                if(isfile){
                    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("提示信息：");
                    builder.setMessage("删除此文件?");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            File file = new File(filename);
                            file.delete();
                            values.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.create().show();
                }
                return false;
            }
        });

    }






    private void loadFiles(String path){
        topview.setText(path);
        File[] files=new File(path).listFiles();
        if(files==null){
            //Log.i("xxx","HGJ??????????????????????????????");
            Toast.makeText(MainActivity.this,"系统文件夹，具体文件不可见！",Toast.LENGTH_LONG).show();

        }else{
            for(int i=0;i<files.length;i++){
                Map<String,Object> map=new HashMap<>();
                String filename=files[i].getName();
                map.put("fullpath",files[i].getAbsolutePath());
                map.put("filename",filename);
                if(files[i].isFile()){
                    map.put("isfile",true);
                    map.put("extname","shadowfile");
                    if(files[i].getName().charAt(0)=='.'){
                        //Log.i("xxx","IOGP>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        map.put("extname","shadowfile");
                    }else if(files[i].getName().indexOf(".")==-1){
                        map.put("extname","file");

                    }else{
                        map.put("extname",filename.substring(filename.lastIndexOf(".") + 1));
                    }
                }else{
                    map.put("isfile",false);
                    map.put("extname","close_dir");
                }
                //Log.i("xxx", map.get("extname").toString());
                values.add(map);
            }
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode==KeyEvent.KEYCODE_BACK){
            Map<String,Object> map=values.get(0);
            if(map.get("hasparent")==null){
                //提示是否退出应用程序
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("提示信息");
                builder.setMessage("亲，真的要残忍的离开吗？");
                builder.setPositiveButton("真的", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //终止应用程序执行
                        finish();
                    }
                });
                builder.setNegativeButton("骗你的", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();

            }else if("true".equals(map.get("hasparent").toString())){
                mylist.performItemClick(mylist.getChildAt(0),0,mylist.getChildAt(0).getId());
            }
            //将返回功能取消
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
