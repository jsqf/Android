package com.buaa.util;

import android.app.Activity;

import com.buaa.activity.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/4/24.
 */
public class Globals  {
    public static int SCREEN_WIDTh;
    public static int SCREEN_HIGHT;
    public static Map<String ,Integer> filetypeImg=new HashMap<>();
    public static void init(Activity a) {
        SCREEN_WIDTh = a.getWindowManager().getDefaultDisplay().getWidth();
        SCREEN_HIGHT = a.getWindowManager().getDefaultDisplay().getHeight();
        filetypeImg.put("close_dir",R.drawable.close_dir);
        filetypeImg.put("open_dir", R.drawable.open_dir);
        filetypeImg.put("bmp", R.drawable.image_file);
        filetypeImg.put("mp3", R.drawable.mp3_file);
        filetypeImg.put("mp4", R.drawable.mp4_file);
        filetypeImg.put("txt", R.drawable.txt_file);
        filetypeImg.put("shadowfile", R.drawable.txt_file);
        filetypeImg.put("file", R.drawable.file);
        filetypeImg.put("rc", R.drawable.file);
        filetypeImg.put("prop", R.drawable.file);
    }
}
